#!/usr/bin/env node

const fs = require('fs');
const uuid = require('uuid');
const bodyParser = require('body-parser')

const express = require('express');

const app = express();
const port = process.argv[2];
const filename = process.argv[3];

if(!port || !filename){
    console.error("Please launch the server with this command : node server.js port filename");
    process.exit(1);
}

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/cities', (req, res,next) => {
    fs.readFile(filename, 'utf8', (err, data) => {
        if(err) {
            res.statusCode = 500;
            next(new Error("file doesn't exist"))
            return 0;
        }
        const myData = JSON.parse(data);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.render('cities', myData);
    });
});

app.post('/city', (req, res, next) => {
    const name = req.body && req.body.name;
    if (!name) {
        res.statusCode = 400;
        next(new Error("Body doesn't contain name"))
        return 0;
    }
    const id = uuid.v1();

    fs.readFile(filename, 'utf8', (err, data) => {
        if(!err) {
            const myData = JSON.parse(data);

            const filtered = myData.cities.filter(city => {
               if (city.name === name) {
                   return city;  
               }
            });
            if (filtered.length){
                res.statusCode = 400;
                next(new Error("name already exist"))
                return 0;
            }
            myData.cities.push({id:id,name:name})
            fs.writeFile(filename, JSON.stringify(myData), err => {
                if (err){
                    return 0;
                }
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/html');
                res.render('cities', myData);
            })
            
        } else {
            fs.writeFile(filename, {cities :[{name:name,id:id}]}, err => {
                if (err){
                    return 0;
                }
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/html');
                res.render('cities', {cities :[{name:name,id:id}]});
            })
        }
        
    });
});
app.put('/city', (req, res, next) => {
    const name = req.body && req.body.name;
    const id = req.body && req.body.id;
    if (!name || !id) {
        res.statusCode = 400;
        next(new Error("Error name or id not found"));
        return 0;
    }    
    fs.readFile(filename, 'utf8', (err, data) => {
        if(err) {
            res.statusCode = 500;
            next(new Error("file doesn't exist"));
            return 0;
        }
        let myData = JSON.parse(data);
        const filtered = myData.cities.filter(city => {
            if (city.id === id) {
                return city;  
            }
        });
        if (!filtered.length){
            res.statusCode = 400;
            next(new Error("id doesn't exist"));
            return 0;
        }
        myData.cities.forEach((element, i) => {
            if (element.id === id) {
                myData.cities[i] = {...element, name:name};
            }
        });
        fs.writeFile(filename, JSON.stringify(myData), err => {
            if (err){
                return 0;
            }
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/html');
            res.render('cities', myData);
        })
    });
});
app.delete('/city/:id', (req, res, next) => {
    const id = req.params && req.params.id;
    if ( !id) {
        res.statusCode = 400;
        next(new Error("Error name not found"));
        return 0;
    }    
    fs.readFile(filename, 'utf8', (err, data) => {
        if(err) {
            res.statusCode = 500;
            next(new Error("file doesn't exist"));
            return 0;
        }
        let myData = JSON.parse(data);
        const filtered = myData.cities.filter(city => {
            if (city.id === id) {
                return city;  
            }
        });
        if (!filtered.length){
            res.statusCode = 400;
            next(new Error("id doesn't exist"));
            return 0;
        }
        const newData  = {cities :myData.cities.filter((element) => {
            if (element.id !== id) {
                return element;
            }
        })};
        fs.writeFile(filename, JSON.stringify(newData), err => {
            if (err){
                return 0;
            }
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/html');
            res.render('cities', newData);
        })
    });
});
app.use((err, _, res, __) =>{
    console.log(err);
    res.render('error', res.statusCode === 400 ? {status : "Error 400", message : err.message} : {status : "Error 500", message : "internal server error"})
})
app.listen(port, ()=>{console.log(`Server listening on port ${port}`);})
